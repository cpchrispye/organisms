
import numpy as np
import random

class Brain():

    @classmethod
    def clone(cls, parent):
        pass

    @classmethod
    def random(cls, shape):
        inst = cls(shape)
        inst.randomise_links()
        return inst

    @classmethod
    def breed(cls, parent_a, parent_b, ratio=0.5, mutate=0.0):
        inst = cls(parent_a.network_shape)
        inst.network_data = []
        for lay_a, lay_b in zip(parent_a.network_data, parent_b.network_data):
            lay_new = (ratio * lay_a) + ((1 - ratio) * lay_b)
            inst.network_data.append(lay_new)

        for i in range(int(mutate*100)):
            for lay in inst.network_data:
                a, b = lay.shape
                ra = random.randrange(0, a)
                rb = random.randrange(0, b)
                lay[ra][rb] = lay[ra][rb] * random.uniform(0.9, 1.1)
                if lay[ra][rb] > 1: lay[ra][rb] = 1
                if lay[ra][rb] < -1: lay[ra][rb] = -1

        return inst

    def __init__(self, shape):

        self.network_shape = shape
        self.state_shape = (10, 5)
        self.state = np.ndarray(self.state_shape)

        self.build_network()

    def build_network(self):
        self.network_data = []
        for i in range(len(self.network_shape)-1):
            a = self.network_shape[i]
            b = self.network_shape[i + 1]
            self.network_data.append(np.ndarray((b, a+1)))


    def randomise_links(self):
        self.network_data = []
        for i in range(len(self.network_shape)-1):
            a = self.network_shape[i]
            b = self.network_shape[i + 1]
            self.network_data.append(np.random.uniform(-1, 1, (b, a+1)))

    def get_layer_hash(self):
        layer_hash = []
        for layer_link in self.network_data:
            layer_hash.append(np.mean(np.absolute(layer_link)))
        return layer_hash



    def think(self, input):
        # SIMPLE MLP
        try:
            af = lambda x: np.tanh(x)  # activation function
            node_layer = np.asarray(input, dtype = float)
            node_layer.shape = (self.network_shape[0], 1)

            for layer_link in self.network_data:
                node_layer = af(node_layer)
                node_layer = np.insert(node_layer, 0, np.array(1.0,), 0)
                node_layer = np.dot(layer_link, node_layer)  # hidden layer

            # for layer_link in self.network_data:
            #     node_layer = np.insert(node_layer, 0, np.array(1.0,), 0)
            #     node_layer = af(np.dot(layer_link, node_layer))  # hidden layer

            return node_layer
        except Exception as e:
            print(str(e))
            raise e


