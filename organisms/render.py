import sys
import pygame
import math
from organisms import utils

clock = pygame.time.Clock()
pygame.init()

BLACK = (0, 0, 0)
GREEN = (20, 255, 140)
GREY = (210, 210 ,210)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
PURPLE = (255, 0, 255)


def center_to_edge(x, y):
    info = pygame.display.Info()
    return [(info.current_w // 2) + x, (info.current_h // 2) + y]


def edge_to_center(x, y):
    info = pygame.display.Info()
    return [x - (info.current_w // 2), y - (info.current_h // 2)]


class Render(object):

    def __init__(self, ):
        self.screen = None
        self.gui = GUI(self)
        self.text = []
        self._status = False

        self.zoom_index = 0

        self.pause_sim = False
        self._drag_start = None
        self.interest_click = None
        self.screen_offset = [0,0]

    @property
    def zoom(self):
        return math.pow(1.3, self.zoom_index)

    def change_zoom(self, change, zoom_pos):
        m_x, m_y  = zoom_pos

        self.screen_offset[0] = m_x
        self.screen_offset[1] = m_y

        self.zoom_index = utils.limit(self.zoom_index + change, -10, 30)

    def running(self):
        return self._status

    def set_res(self, res):
        self.resolution = res
        x, y = self.resolution
        self.res_y = y
        self.res_x = x

        self.gui.set_size()

    def start(self):
        self.screen = pygame.display.set_mode(self.resolution, pygame.RESIZABLE)
        self._status = True

    def stop(self):
        try:
            pygame.display.quit()
        except:
            pass
        self._status = False

    def clear_world(self):
        self.screen.fill(WHITE)

    def draw_gui(self):
        self.gui.draw()

    def mark_point(self, point, color=PURPLE):
        x, y = point
        xy = (
                  val_to_res(x, self.res_x, self.zoom, self.screen_offset[0]),
                  val_to_res(y, self.res_y, self.zoom, self.screen_offset[1], True)
              )
        print(xy)
        pygame.draw.circle(self.screen, color, xy, 20)

    def show(self):
        for text_data in self.text:
            text, xy = text_data
            self.screen.blit(text, xy)
        self.text = []

        self.gui.draw()

        pygame.display.flip()
        self.event_handle()

    def wait(self, fps):
        clock.tick(fps)

    def get_organism_sprite(self, radius):
        return OrganismSprite(self, radius)

    def event_handle(self):
        self.interest_click = None

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit(0)
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                sys.exit(0)
            elif event.type == pygame.MOUSEBUTTONDOWN:
                x, y = self.screen_offset
                if event.button == 4:
                    self.change_zoom( 1, (x,y))

                elif event.button == 5:
                    self.change_zoom( -1, (x,y))

                elif event.button == 1:
                    x, y = pygame.mouse.get_pos()
                    self._drag_start = (res_to_val(x, self.res_x, self.zoom, self.screen_offset[0]),
                                        res_to_val(y, self.res_y, self.zoom, self.screen_offset[1], True))

                elif event.button == 3:
                    x, y = pygame.mouse.get_pos()
                    self.interest_click = (res_to_val(x, self.res_x, self.zoom, self.screen_offset[0]),
                                           res_to_val(y, self.res_y, self.zoom, self.screen_offset[1], True))


            elif event.type == pygame.MOUSEBUTTONUP:
                if event.button == 1:
                    self._drag_start = None

            elif event.type == pygame.KEYDOWN:
                x, y = self.screen_offset
                step = 50
                if event.key == pygame.K_DOWN:
                    self.change_zoom(0, (x, y-step))
                elif event.key == pygame.K_UP:
                    self.change_zoom(0, (x, y+step))
                elif event.key == pygame.K_LEFT:
                    self.change_zoom(0, (x-step, y))
                elif event.key == pygame.K_RIGHT:
                    self.change_zoom(0, (x+step, y))
                elif event.key == pygame.K_SPACE:
                    self.pause_sim = not self.pause_sim

            elif event.type == pygame.VIDEORESIZE:
                # The main code that resizes the window:
                # (recreate the window with the new size)
                self.screen = pygame.display.set_mode((event.w, event.h),
                                                       pygame.RESIZABLE)
                self.set_res((event.w, event.h))

        if self._drag_start is not None:
            x, y = pygame.mouse.get_pos()
            self.screen_offset[0] -=  res_to_val(x, self.res_x, self.zoom, self.screen_offset[0]) - self._drag_start[0]
            self.screen_offset[1] -= res_to_val(y, self.res_y, self.zoom, self.screen_offset[1], True) - self._drag_start[1]
            self._drag_start = (res_to_val(x, self.res_x, self.zoom, self.screen_offset[0]),
                                res_to_val(y, self.res_y, self.zoom, self.screen_offset[1], True))

    def text_to_screen(self, text, x, y, size=50,
                       color=(200, 000, 000), font_type='Comic Sans MS'):

        try:
            text = str(text)
            font = pygame.font.SysFont(font_type, size)
            text_render = font.render(text, True, color)
            self.text.append((text_render, (x,y)))
        except Exception as e:
            print('Font Error, saw it coming')
            raise e

    def get_agent_sprite(self, name, size):
        if name == 'organism':
            return OrganismSprite(self, size)

        elif name == 'food':
            return FoodSprite(self, size)


class GUI(object):

    def __init__(self, render):
        self.render = render
        self.background_color = 139, 177, 237
        self.color_key = 125, 255, 0
        self._selected_organism = None

    def set_size(self, width=300):
        self.image = pygame.Surface((width, self.render.res_y))
        self.image.fill(self.color_key)
        self.image.set_colorkey(self.color_key)
        self.rect = self.image.get_rect()

        self.rect.right = self.render.res_x
        self.rect.top = 0

    def show_organism(self, organism):
        self._selected_organism = organism

    def no_show_organism(self):
        self._selected_organism = None

    def draw(self):
        if self._selected_organism is not None:
            self.image.fill(self.color_key)
            pygame.draw.rect(self.image, self.background_color, self.image.get_rect())
            self.render.screen.blit(self.image, self.rect)



def res_to_val(dv, res, scale=1, offset_center=0, flip=False):

    if flip:
        dv = res - dv
    val = ((dv - (res /2))/scale) + offset_center
    return val

def val_to_res(val, res, scale=1, offset_center=0, flip=False):
    dv = ((val - offset_center) * scale) + (res /2)
    if flip:
        dv = res - dv
    return int(dv)

def scaled_pos(x, y, scale=1, offset_center=(0,0), flip_y=False):
    info = pygame.display.Info()
    ox, oy = offset_center
    dx = ((x - ox) * scale) + (info.current_w /2)
    dy = ((y - oy) * scale) + (info.current_h /2)

    if flip_y:
        info = pygame.display.Info()
        dy = info.current_h - dy
    return dx, dy


class AgentSprite(pygame.sprite.Sprite):
    color_key = WHITE
    def __init__(self, render, size):
        # Call the parent class (Sprite) constructor
        super().__init__()

        self.render = render
        self.screen = self.render.screen
        self.size = size
        self._old_scaled_size = size

        self._x = 0
        self._y = 0

        self.set_size(size)

    def set_size(self, size):
        self._old_scaled_size = size
        self.image = pygame.Surface((size, size))
        self.image.fill(self.color_key)
        self.image.set_colorkey(self.color_key)
        self.rect = self.image.get_rect()
        self.rect.centerx = val_to_res(self.x, self.render.res_x, self.render.zoom, self.render.screen_offset[0])
        self.rect.centery = val_to_res(self.y, self.render.res_y, self.render.zoom, self.render.screen_offset[1], True)

    def get_drawing_items(self):
        if self._old_scaled_size != self.scaled_size:
            self.set_size(self.scaled_size)
        return self.image, self.rect

    @property
    def scaled_size(self):
        return int(self.size * self.render.zoom)

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, val):
        self._x = val
        self.rect.centerx = val_to_res(val, self.render.res_x, self.render.zoom, self.render.screen_offset[0])

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, val):
        self._y = val
        self.rect.centery = val_to_res(val, self.render.res_y, self.render.zoom, self.render.screen_offset[1], True)

    def draw(self):
        pass

    def show(self):
        self.screen.blit(self.image, self.rect)

    def remove(self):
        pass


class OrganismSprite(AgentSprite):

    def __init__(self, render, size):
        super().__init__(render, size)
        self.radius = size // 2
        self.orientation = 0
        self.eye_orientation = 0
        self._head_color = WHITE

        self.color = PURPLE

    def die(self, shrink=0):
        self.color = GREY

    def set_skin(self, rgb):
        self.color = rgb

    def set_head(self, rgb):
        self._head_color = rgb

    def draw(self):

        image, rect = self.get_drawing_items()

        image.fill(WHITE)
        radius = self.scaled_size // 2
        hat_radius = radius // 2
        eye_size = radius // 4
        eye_radius = (radius // 2) - eye_size

        pygame.draw.circle(image, self.color, (radius, radius), radius)
        pygame.draw.circle(image, self._head_color, (radius, radius), hat_radius)

        x = (math.cos(self.orientation) * radius) + radius
        y = rect.height - ((math.sin(self.orientation) * radius) + radius)

        sx = (math.cos(self.orientation) * hat_radius) + radius
        sy = rect.height - ((math.sin(self.orientation) * hat_radius) + radius)

        pygame.draw.line(image, WHITE, (sx, sy), (x, y), 2)


        x = (math.cos(self.eye_orientation) * eye_radius) + radius
        y = rect.height - ((math.sin(self.eye_orientation) * eye_radius) + radius)
        pygame.draw.circle(image, BLACK, (round(x), round(y)), eye_size)

        self.screen.blit(image, rect)


class FoodSprite(AgentSprite):
    # This class represents a car. It derives from the "Sprite" class in Pygame.

    def __init__(self, render, size):
        super().__init__(render, size)
        self.radius = size // 2
        self.orientation = 0

        self.set_size(size)

    def draw(self):
        radius = self.scaled_size // 2
        image, rect = self.get_drawing_items()
        pygame.draw.circle(image, GREEN, (radius, radius), radius)
        self.screen.blit(image, rect)


def draw_circle(screen, x, y, size, color):
    pygame.draw.circle(screen, color, [x, y], size)


