import random
import time
from organisms.render import Render
from organisms.physical import WorldPhysics
from organisms.agents import Organism, Food


class World(object):

    def __init__(self, map_width, map_height):
        self.map_height = map_height
        self.map_width  = map_width
        self.map_size   = (map_width, map_height)

        self.fps = 60
        self.start_time = time.time()
        self.sim_time = 0
        self.sim_count = 0
        self.generation = 0
        self.count = 0

        self.organisms = list()
        self.food      = list()

        self.renderer = Render()
        self.physics  = WorldPhysics()


        self._food_qty = 10
        self.all_organisms = list()
        self.organisms = list()
        self.food_site = list()

        self.think_qty = 30
        self._current_org_index = 0

    def add_agent(self, agent):
        if isinstance(agent, Organism):
            self.organisms.append(agent)
            self.all_organisms.append(agent)
        elif isinstance(agent, Food):
            self.food.append(agent)
        else:
            return False

        agent.set_world(self)
        agent.set_physics(self.physics)
        agent.set_render(self.renderer)
        agent.active = True
        return True

    def remove_agent(self, agent):
        if isinstance(agent, Organism):
            if agent in self.organisms:
                self.organisms.remove(agent)
        elif isinstance(agent, Food):
            if agent in self.food:
                self.food.remove(agent)
        else:
            return False

        agent.remove_physics()
        agent.remove_render()

        return True

    def remove_all(self):
        for agent in self.organisms:
            agent.remove_physics()
            agent.remove_render()

        for agent in self.food:
            agent.remove_physics()
            agent.remove_render()

        self.all_organisms = list()
        self.organisms     = list()
        self.food          = list()

    def food_qty(self, qty):
        self._food_qty = qty

    def simulate_tick(self, multiplyer=1):
        if not self.renderer.pause_sim:
            self.sim_count += 1

            for i in range(multiplyer):
                self.physics.step(self.fps*multiplyer)
            self.sim_time += 1.0/self.fps

            for food in self.food:
                if not food.active:
                    self.remove_agent(food)

            org_count = 0
            for org in self.organisms:
                if org.alive:
                    org_count += 1

            clamp = lambda n, minn, maxn: max(min(maxn, n), minn)
            food_qty = clamp(self._food_qty * int((50**2) / (org_count**2)), 10, self._food_qty)

            food_required = food_qty - len(self.food)
            if food_required > 0:
                for _ in range(food_required):
                    new_food = Food(random.randrange(-self.map_width, self.map_width), random.randrange(-self.map_height, self.map_height), 10)
                    self.add_agent(new_food)

            for org in self.organisms:
                org.update()
                if not org.active:
                    self.remove_agent(org)

            for org in self.organisms:
                if org.alive:
                    org.see_world()
                    org.process()
                    org.control()

            #self._current_org_index = (self.think_qty + self._current_org_index) % len(self.organisms)


    @property
    def rendering(self):
        return self.renderer.running()

    def render_on(self, resolution=None):
        if resolution is None:
            resolution = (self.map_width, self.map_height)

        self.renderer.stop()
        self.renderer.set_res(resolution)
        self.renderer.start()

    def render_off(self):
        self.renderer.stop()

    def draw(self):

        if self.rendering:

            if self.renderer.interest_click is not None:
                orgs = self.physics.get_agent_at(self.renderer.interest_click)
                if orgs:
                    self.renderer.gui.show_organism(orgs[1])
                else:
                    self.renderer.gui.no_show_organism()


            #self.renderer.event()
            self.renderer.clear_world()

            pop_size = 0
            for organism in self.organisms:
                organism.draw()
                if organism.alive:
                    pop_size += 1

            for food in self.food:
                #food.update()
                food.draw()

            text = "Time Running {}, Generation {}, Population {}".format(
                int(time.time() - self.start_time),
                self.generation,
                pop_size)

            self.renderer.text_to_screen(text, 5, 5, 30, color=(50, 50, 50), font_type='Arial')

            # if self.renderer.interest_click is not None:
            #     self.renderer.mark_point(self.renderer.interest_click)

            self.renderer.show()

            self.renderer.wait(self.fps)


    class FoodSite(object):

        def __init__(self, x, y, size, qty=50):

            self.x = x
            self.y = y
            self.size = size
            self.qty = qty
            self.last_add_time = 0

            self.food = []
            while len(self.food) < self.qty:
                self.add_food()

        def update(self):
            if len(self.food) < self.qty and (time.time() - self.last_add_time > 5):
                self.add_food()

        def remove(self, food):
            try:
                self.food.remove(food)
            except IndexError:
                pass

        def add_food(self):
            x = (random.random() - 0.5) * self.size
            y = (random.random() - 0.5) * self.size
            new_food = Food(self.x + x, self.y + y , 10)
            new_food.site = self
            self.food.append(new_food)
            self.last_add_time = time.time()

