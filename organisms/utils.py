import math

def limit(val, from_val, to_val):
    return max(min(val, to_val), from_val)

def abs_angle(rad):

    limited = rad % math.tau
    if limited < 0:
        return math.tau - limited
    return limited

def dist(x1, y1, x2, y2):
    return math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

def heading(x1, x2, y1, y2, rot):
    d_x = x2 - x1
    d_y = y2 - y1
    theta_d = abs_angle(math.atan2(d_y, d_x)) - rot
    return theta_d