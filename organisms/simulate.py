
import random
import time
from organisms import ai
from organisms import agents
import math
from organisms.world import World

def simulate(world, period):


    while period==0 or world.sim_time < period:

        world.simulate_tick(multiplyer=2)
        world.draw()

        if len([org for org in world.organisms if org.alive]) <= 2:
            return None
            pass


def get_non_uniform_int(frm, to, step=0.5):

    n = to - frm
    r = step
    a = 1
    if step < 1:
        a = 1/ (r ** n)
    sum = int(math.ceil(a*(1-(r**n))/(1-r)))
    pre_index = random.randrange(0, sum)
    index = min(int(math.log10(1 - (pre_index * (1 - r) / a)) / math.log10(r)), n)

    return frm + index


def breed(parent_a, parent_b, ratio=0.5, mutate=0.0):
    elements = []
    for ele_a, ele_b in zip(parent_a, parent_b):
        ele_m = (ratio * ele_a) + ((1 - ratio) * ele_b)
        if random.random() < mutate:
            ele_m *= random.uniform(0.9, 1.1)
        elements.append(ele_m)
    return elements

def breed_next_gen(world, old_organisms, qty):
    quota_pc = 0.3
    mutate_rate = 0.30
    quota = max(int(qty * quota_pc), 2)


    old_organisms.sort(key=lambda x: x.age, reverse=True)
    if old_organisms[quota].alive == True:
        old_organisms.sort(key=lambda x: x.energy, reverse=True)
        mutate_all = False
    else:
        mutate_all = True

    parents = old_organisms[:quota]

    organisms_new = []
    for parent in parents:
        new_me = agents.Organism(parent.x, parent.y, parent.size)
        new_me.brain = parent.brain
        organisms_new.append(new_me)


    while len(organisms_new) < qty:

        # SELECTION (TRUNCATION SELECTION)

        mindex = get_non_uniform_int(0, quota, 0.8)
        findex = get_non_uniform_int(0, quota, 0.8)

        if mindex == findex:
            continue

        org_1 = parents[mindex]
        org_2 = parents[findex]

        # CROSSOVER
        crossover_weight = random.random()


        spawn_dist = int(org_1.size * 2)
        new_me = agents.Organism(random.randrange(-world.map_width, world.map_width),
                                 random.randrange(-world.map_height, world.map_height),
                                 org_1.size)
        # random.randrange(int(org_1.x - spawn_dist), int(org_1.x + spawn_dist)),
        # random.randrange(int(org_1.y - spawn_dist), int(org_1.y + spawn_dist)),

        if mutate_all:
            mutate = random.random()
        elif random.random() < mutate_rate:
            mutate = random.random()
        else:
            mutate = 0

        #new_me.skin = breed(org_1.skin, org_2.skin, ratio=0.5, mutate=1.0)

        new_me.brain = ai.Brain.breed(org_1.brain, org_2.brain, crossover_weight, mutate)
        organisms_new.append(new_me)

    return organisms_new


def run_gen():
    world_size_x = 1500
    world_size_y = 1000

    org_start_num = 50

    world = World(world_size_x//4, world_size_y//4)
    world.fps = 25

    world.render_on((world_size_x, world_size_y))
    for _ in range(org_start_num):
        agent = agents.Organism(random.randrange(-world_size_x//2, world_size_x//2), random.randrange(-world_size_y//2, world_size_y//2), 20)
        #agent.skin = [random.random(), random.random(), random.random()]
        world.add_agent(agent)

    world.food_qty(40)

    gen = 0
    while True:
        print(time.time() - world.start_time)
        world.generation = gen
        world.start_time = time.time()
        world.sim_time = 0

        simulate(world, 0)

        new_gen = breed_next_gen(world, world.all_organisms, org_start_num)

        world.remove_all()

        for agent in new_gen:
            world.add_agent(agent)
        gen += 1


if __name__ == '__main__':

    run_gen()