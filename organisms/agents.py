from abc import ABC, abstractmethod, ABCMeta
from organisms.render import OrganismSprite
from organisms.physical import OrganismPhysical
from organisms.ai import Brain
import random
import time
from organisms.utils import *


class Agent(metaclass=ABCMeta):

    RenderClass   = OrganismSprite
    PhysicalClass = OrganismPhysical

    def __init__(self):
        self.x = 0
        self.y = 0
        self._r = 0
        self.size = 0

        self.active = False
        self.world = None

        self.physics_engine = None
        self.render_engine  = None

    @property
    @abstractmethod
    def name(self):
        pass

    def set_physics(self, physics_engine):
        self.physics_engine = physics_engine

        self.physics = physics_engine.get_agent_physics(self.name, self.size) #type: AgentPhysical
        self.physics.agent = self
        self.physics.position = self.x, self.y

    def set_render(self, render_engine):
        self.render_engine = render_engine
        self.sprite = render_engine.get_agent_sprite(self.name, self.size)

    def set_world(self, world):
        self.world = world

    def remove_physics(self):
        self.physics_engine = None
        self.physics.remove()
        self.physics = None

    def remove_render(self):
        self.render_engine = None
        self.sprite.remove()
        self.sprite = None

    def update(self):
        pos = self.physics.position
        self.x = pos.x
        self.y = pos.y

    def draw(self, ):
        self.sprite.x = self.x
        self.sprite.y = self.y
        self.sprite.orientation = self.rotation
        self.sprite.draw()

    @property
    def rotation(self):
        return self._r

    @rotation.setter
    def rotation(self, val):
        self._r = abs_angle(val)

    @abstractmethod
    def process(self):
        pass

    @abstractmethod
    def control(self):
        pass

    def collision_callback(self, org):
        pass


class Organism(Agent):

    def __init__(self, x, y, size):
        super().__init__()

        self.x        = x
        self.y        = y
        self.size     = size
        self.rotation = random.random() * 7

        self.energy        = 2000
        self.alive         = True
        self.food_heading  = 0
        self.food_distance = 0
        self.dob           = time.time()
        self.age           = 0
        self.shrink        = 0

        self.focus_direction = 0

        self.red   = 0
        self.blue  = 0
        self.green = 0

        self.eye_dist  = 300
        self.eye_res   = 10
        self.eye_food  = [0] * self.eye_res
        self.eye_org   = [0] * self.eye_res
        self.eye_focus = [-1, -1, -1, -1]

        self.control_body = None

        self.brain = Brain.random(((10 + 4 + 3) + (2*self.eye_res), 20, 12, 7))

        self.brain_hash = self.brain.get_layer_hash()
        self.skin = [0.5]*3
        for i, cl in enumerate(self.brain_hash):
            if i >= len(self.skin):
                break
            self.skin[i] = (cl * 10) - int(cl * 10)

        self._child = None

    @property
    def name(self):
        return "organism"

    def update(self):
        super().update()
        if not self.alive:
            time_after_death = time.time() - self.age
            if time_after_death > 30:
                self.active = False


    def see_world(self):
        agents = self.physics.get_agents(500, False, False)
        agents.sort(key=lambda x: x[0], reverse=True)

        const = self.eye_res/math.tau

        self.eye_food = [0] * self.eye_res
        self.eye_org  = [0] * self.eye_res
        self.eye_focus = [-1.0]*7

        for dist, agent in agents:
            angle = heading(self.x, agent.x, self.y, agent.y, self.rotation)
            if agent.name == 'food':
                self.eye_food[int(angle * const)] = (1 - (dist / self.eye_dist))
            else:
                index = int(angle * const)
                self.eye_org[index] = (1 - (dist / self.eye_dist))
                self.eye_focus
                if int(self.focus_direction * const) == index:
                    self.eye_focus = agent.skin + [self.energy/10000.0 if agent.alive else -1.0] + [agent.red, agent.green, agent.blue]


    def draw(self):
        sprite = self.sprite
        sprite.x = self.x
        sprite.y = self.y
        sprite.orientation = self.rotation
        sprite.eye_orientation = self.focus_direction
        if self.alive:
            sprite.set_skin((int(self.skin[0] * 255), int(self.skin[1] * 255), int(self.skin[2] * 255)))
            sprite.set_head((self.red, self.green, self.blue))
        else:
            sprite.die()

        sprite.draw()

    def process(self):
        if self.alive:
            in_energy = limit(self.energy/10000.0, 0, 1)

            x, y = self.physics.body.velocity
            vel = math.hypot(x, y)
            in_speed = limit(vel/100.0, 0, 1)
            in_angle_vel = (math.atan2(y, x) - self.rotation) / (math.pi * 2)

            in_red   = limit(self.red / 255.0, 0, 1)
            in_green = limit(self.green / 255.0, 0, 1)
            in_blue  = limit(self.blue / 255.0, 0, 1)

            out = self.brain.think(self.eye_focus + self.eye_food + self.eye_org  +
                                   [in_energy, in_speed, in_angle_vel, in_red, in_green, in_blue, self.x/1000, self.y/1000, self.rotation/math.tau, self.focus_direction/math.tau])

            self.control_body = tuple(out.flatten())

    def control(self):
        if self.alive:

            rotation, focus_rot, speed, red, green, blue, mate = self.control_body
            self.rotation += rotation
            self.focus_direction = abs_angle(self.focus_direction + focus_rot)

            speed_mul = 100
            vel_x = math.cos(self.rotation) * speed * speed_mul
            vel_y = math.sin(self.rotation) * speed * speed_mul
            self.physics.impulse(vel_x, vel_y)

            self.red   = (self.red   + (red * 255.0)) % 255
            self.green = (green * 255.0) % 255
            self.blue  = (blue * 255.0) % 255


            self.energy -= ((speed * 2) ** 4)/2.0
            self.energy -= ((rotation * 3) ** 4)/3.0

            self.age = time.time()

            if mate > 0.5:
                self.mate()

            self.energy -= 6 * sum(self.brain_hash)
            if self.energy < 0:
                self.die()

            if (self.age - self.dob) > 60*10:
                print("died of old age")
                self.die()

    def die(self):
        self.alive = False

    def mate(self):
        partners = self.physics.get_agents(self.size*2, organism=True)
        #print('mating')
        self.energy -= 5

        if self.energy < 2000:
            return False

        if len(partners) == 0:
            return False

        partner = partners[0][1]
        if partner.alive:
            child = Organism(self.x, self.y, self.size)
            child.brain = Brain.breed(self.brain, partners[0][1].brain, random.random(), random.random()/0.1)
            self._child = child
            self.world.add_agent(child)
            self.energy -= 2000
            print('child added')
        return True


class Food(Agent):

    def __init__(self, x, y, size):
        super().__init__()
        self.x = x
        self.y = y
        self.size = size
        self.rotation = 0
        self.site = None

    @property
    def name(self):
        return "food"

    def process(self):
        pass

    def control(self, physics):
        pass


    def collision_callback(self, org):
        if org.active:
            org.energy += 1000
        self.active = False


