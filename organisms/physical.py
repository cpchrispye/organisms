import pymunk
COLL_TYPE_ORG = 50
COLL_TYPE_FOOD = 100


class WorldPhysics(object):

    def __init__(self):
        self.space = pymunk.Space() #type: pymunk.Space
        self.space.damping = 0.0001
        food_col_handler = self.space.add_collision_handler(COLL_TYPE_FOOD, COLL_TYPE_ORG)
        food_col_handler.begin = food_collision_handler

    def step(self, fps):
        self.space.step(1.0/fps)

    def get_agent_at(self, point, radius=0, food=False, organism=False):
        mask = 0
        mask |= COLL_TYPE_ORG if food else 0
        mask |= COLL_TYPE_FOOD if organism else 0
        mask ^= pymunk.ShapeFilter.ALL_MASKS
        query = self.space.point_query_nearest(point, radius, pymunk.ShapeFilter(mask=mask))
        if query is None:
            return None
        return query.distance, query.shape.agent

    def get_agent_physics(self, name, size):

        if name == 'organism':
            return OrganismPhysical(self, size)

        elif name == 'food':
            return FoodPhysical(self, size)


class AgentPhysical(object):
    TYPE = 0

    def __init__(self, world_physics):
        self.world_physics = world_physics
        self.space = self.world_physics.space

        self.body = None
        self.shape =  None

        self._agent = None

    @property
    def x(self):
        return self.body.position.x

    @x.setter
    def x(self, val):
        self.body.position = val, self.y

    @property
    def y(self):
        return self.body.position.y

    @y.setter
    def y(self, val):
        self.body.position = self.x, val

    @property
    def position(self):
        return self.body.position

    @position.setter
    def position(self, val):
        self.body.position = val

    def get_agents(self, radius, food=False, organism=False):
        mask = 0
        mask |= COLL_TYPE_ORG if food else 0
        mask |= COLL_TYPE_FOOD if organism else 0
        mask ^= pymunk.ShapeFilter.ALL_MASKS
        shapes = self.space.point_query((self.x, self.y), radius, pymunk.ShapeFilter(mask=mask))
        return [(s.distance, s.shape.agent) for s in shapes if s.shape.agent is not self.agent]

    def make_body_circle(self, radius, mass=1):
        self.radius = radius

        moment = pymunk.moment_for_circle(mass, 0, radius)
        body = pymunk.Body(mass, moment)
        shape = pymunk.Circle(body, radius)
        shape.collision_type = self.TYPE
        shape.filter = pymunk.ShapeFilter(categories=self.TYPE)

        self.space.add(body, shape)
        self.body = body
        self.shape =  shape

    def make_body_square(self, height, width, mass=1):
        raise Exception('make_body_square not yet supported')

    def impulse(self, vel_x, vel_y):
        self.body.apply_impulse_at_local_point((vel_x, vel_y))

    def remove(self):
        self.space.remove(self.body, self.shape)

    @property
    def agent(self):
        return self._agent

    @agent.setter
    def agent(self, val):
        self._agent = val
        self.shape.agent = val


class OrganismPhysical(AgentPhysical):
    TYPE = COLL_TYPE_ORG

    def __init__(self, world_physics, size):
        super().__init__(world_physics)
        self.make_body_circle(size//2)


class FoodPhysical(AgentPhysical):
    TYPE = COLL_TYPE_FOOD

    def __init__(self, world_physics, size):
        super().__init__(world_physics)
        self.make_body_circle(size//2)


def food_collision_handler(arbiter, space, data):
    #type: (pymunk.arbiter, pymunk.Space, dict) -> bool
    shapes = arbiter.shapes
    food, org = shapes #type: pymunk.Shape
    food.agent.collision_callback(org.agent)
    return False
