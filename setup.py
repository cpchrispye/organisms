from setuptools import setup

setup(
        name='organisms',
        version='0.0.1',
        packages=[
            'organisms',
        ],
        entry_points={
            'console_scripts': [
                'organisms=organisms.simulate:run_gen'
            ],
        },
        url='',
        license='',
        author='cmpye',
        author_email='',
        description='Tools useful for running automated tests',
        install_requires=[
                            'pygame==1.9.3',
                            'pymunk==5.3.2',
                            'numpy==1.14.5',
                         ],
        include_package_data=True,
)
